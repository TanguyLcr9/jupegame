﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;




public class GameManager : MonoBehaviour
{
    public static GameManager Instance;//Instance staique pour récuperer les fonctions du script dans les autres

    public float Score;
    public float ScoreToReach;
    public float ScoreToNext;
    public float time;
    public float maxTimer;
    public int currentLevel;

    [Header("UI")]
    public TextMeshProUGUI score_Tmp;
    public TextMeshProUGUI time_Tmp;
    public GameObject[] cullottes_Reward;
    public Image slider;

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(this.gameObject);//si instance existe, on détruit pour eviter problèmes
        }
        else
        {
            Instance = this;//Si aucune instance, celle ci est l'instance
        }
    }

    private void Start()
    { 
        AddScore(0);//initialise le scoring
    }

    private void Update()
    {
        time += Time.deltaTime;
        time_Tmp.text = (int)time + " / " +maxTimer;//affichage du temps sur le temps max;

        if(time >= maxTimer)//si on arrive au temps max on active la fin et on désactive le jeu
        {
            PlayerController.Instance.gameObject.SetActive(false);
            this.enabled = false;
            GetComponent<EndGame>().enabled = true;
            transform.GetChild(0).gameObject.SetActive(false);
        }

    }

    /// <summary>
    /// Fonction ajoutant du score ou décremetant avec verification des passages de niveaux
    /// </summary>
    /// <param name="addScore"> score à ajouter, peut être négatif </param>
    public void AddScore(float addScore = 0)
    {
        Score += addScore * 500;//incrementation
        
        score_Tmp.color = addScore < 0? Color.red : Color.green;//coloris du texte change en fonction d'incrementation ou décrementation

        score_Tmp.text = "Score : " + (int)Score;//affichage du texte

        slider.fillAmount = ExtensionsMethods.Remap(Score, 0, ScoreToReach, 0, 1);//remplissage de la jauge

        currentLevel = (int)(Score/(ScoreToReach / (cullottes_Reward.Length - 1)));//determine le level en fonction du score
        currentLevel = Mathf.Clamp(currentLevel, 0, cullottes_Reward.Length);//clamp du current level entre 0 et 4



        //Affichage des cullotes reward en fonction du score
        for (int i = 0; i < cullottes_Reward.Length; i++)
        {
            if (i <= currentLevel)
            {
                cullottes_Reward[i].GetComponent<Animator>().SetBool("isCurrent",true);
            }
            else
            {
                cullottes_Reward[i].GetComponent<Animator>().SetBool("isCurrent", false);
            }
        }
        WalkerMananger.Instance.currentLevel = currentLevel;//donner le level au walkermanager
        
        if (Score > ScoreToReach)
        {
            cullottes_Reward[cullottes_Reward.Length - 1].GetComponent<Animator>().SetBool("isCurrent", true);//derniere cullotte si on dépasse le score max
        }
    }
}
