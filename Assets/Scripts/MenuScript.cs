﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class MenuScript : MonoBehaviour
{
    public float timer;
    public bool attractMode;
    public GameObject Menu, AttractMode;
    public bool button;
    public bool lastButton;
    public bool buttonPressed;
    public Coroutine mycoroutine;

    /// <summary>
    /// Fonction engageant le jeu
    /// </summary>
    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }



    private void Update()
    {
        if (attractMode)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //desactive le attractmode
                timer = 0;
                if (mycoroutine == null)
                    mycoroutine = StartCoroutine(ChangeMode(false));
            }
            if (buttonPressed)//button arduino
            {
                //desactive le attractmode
                timer = 0;
                if (mycoroutine == null)
                    mycoroutine = StartCoroutine(ChangeMode(false));
                Debug.Log("presseed");
            }
        }
        else 
        {
                if (Input.GetKeyDown(KeyCode.Space))
            {
                //lance le jeu
                StartGame();
            }
            if (buttonPressed)//button arduino
            {
                //lance le jeu
                StartGame();
            }
        }

        if (timer >= 10 && !attractMode)
        {
            if(mycoroutine == null)
            mycoroutine = StartCoroutine(ChangeMode(true));
            
        }

        buttonPressed = button;
        timer += Time.deltaTime;
    }

    /// <summary>
    /// coroutine true pour afficher le attractmode false pour le menu
    /// </summary>
    /// <param name="state"></param>
    /// <returns></returns>
    IEnumerator ChangeMode(bool state)
    {
        if (!state)
        {
            Menu.SetActive(!state);
            AttractMode.SetActive(state);
        }
        if (state)
        {
            
            attractMode = state;
        }


        Menu.transform.GetChild(1).GetComponent<CanvasGroup>().DOFade(state ? 0 : 1, 1f);
        yield return Menu.transform.GetChild(0).GetComponent<CanvasGroup>().DOFade(state ? 0 : 1, 1f).WaitForCompletion();//fait un fade et attend la fin

        if (!state)
        {
            attractMode = state;
        }
        if (state)
        {
            AttractMode.SetActive(state);
            Menu.SetActive(!state);
        }

        mycoroutine = null;// désasigné la coroutine pour pouvoir en refaire derrière
    }

    /// <summary>
    /// Fonction prévue par Ardity pour lire les messages si il reçoit
    /// </summary>
    /// <param name="msg"></param>
    void OnMessageArrived(string msg)
    {
        lastButton = button;

        ArduinoData json = JsonUtility.FromJson<ArduinoData>(msg);

        button = json.button;

        //condition pour créer l'event du button pressed
        if (button == true  &&  lastButton == false) {
            buttonPressed = true;
        }
        else
        {
            buttonPressed = false;
        }

    }
}
