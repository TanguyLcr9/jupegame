﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

//Script de fain de jeu
public class EndGame : MonoBehaviour
{
    public GameObject EndPanel;
    public float scoreFinal;
    public float timer;
    
    bool button, buttonPressed, lastButton;

    public TextMeshProUGUI scoreFinalTmp;
    public TextMeshProUGUI infoText;
    public TextMeshProUGUI rewardTitle;
    public string[] name;
    public SerialController SerialControl;


    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > 30)
        {
            SceneManager.LoadScene(0);
        }else
        if (timer > 5)
        {
            if (Input.GetKeyDown(KeyCode.Space) || buttonPressed)
            {
                SceneManager.LoadScene(0);
            }
            infoText.gameObject.SetActive(true);//affiche le message "press button to continue"
        }
        
        
        buttonPressed = button;
    }

    private void OnEnable()//quand activé il active le panel de fin et désactive le jeu avec un son joué
    {
        infoText.gameObject.SetActive(false);//desactive le message "press button to continue"
        EndPanel.SetActive(true);
        scoreFinal = GetComponent<GameManager>().Score;
        scoreFinalTmp.text += " " + (int)scoreFinal;//affichage du score final
        SerialControl.messageListener = this.gameObject;//le message listener du serial controle doit être cet objet
        ScoreSaver.Instance.SaveScore((int)GetComponent<GameManager>().Score);
        GetComponent<AudioSource>().Play();
        try
        {
            rewardTitle.text = name[GetComponent<GameManager>().currentLevel];
        }
        catch (System.Exception)
        {

        }
    }

    /// <summary>
    /// Fonction prévut par Ardity pour lire les messages si il reçoit
    /// </summary>
    /// <param name="msg"></param>
    void OnMessageArrived(string msg)
    {
        lastButton = button;
        ArduinoData json = JsonUtility.FromJson<ArduinoData>(msg);
        button = json.button;

        //condition pour créer l'event du button pressed
        if (button == true && lastButton == false)
        {
            buttonPressed = true;
        }
        else
        {
            buttonPressed = false;
        }

    }
}
