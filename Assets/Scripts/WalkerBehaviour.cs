﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WalkerBehaviour : MonoBehaviour
{
    public float speed;
    public float fleepSpeed;
    public float scoreSpeed;
    public float MalusScore;
    public bool InverseWalk;
    public SpriteRenderer body, leg1, leg2, calf1, calf2, shoes1, shoes2, skirt, arm1, arm2;
    public float timerMax;
    public float timerWarning;

    Animator animator;
    public AudioSource screamSource, warningSource;
    public AudioClip[] screamClips, warningClips;
    public ParticleSystem plus, moins;

    Coroutine warningCoroutine;
    public static Coroutine shakeCoroutine;
    public bool isKilt;

    public static Tween mytween;

    bool isAspiring, isWarning, isFleeing;
    float timerAspiring;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        try{
            screamSource.clip = screamClips[Random.Range(0, screamClips.Length)];
            warningSource.clip = warningClips[Random.Range(0, warningClips.Length)];
        }catch (System.Exception)
        {
            Debug.Log("NoAudioClips");
        }
    }

    // Update is called once per frame
    void Update()
    {
        //déplacement en local sur l'axe X avec valeure constante
        if (!isWarning)
        {
            transform.localPosition += (InverseWalk ? Vector3.right : Vector3.left) * Time.deltaTime * speed;//déplacement si pas en alerte
        }

        if (!screamSource.isPlaying)
        {
            if (transform.localPosition.x < -15 || transform.localPosition.x > 15)
            {
                Destroy(this.gameObject);//si dépasse le seuil en x
            }
        }

        if (isAspiring)//si on aspire
        {
            //reset des emmisions
            var em_Plus = plus.emission;
            em_Plus.rateOverTime = 0;
            var em_Moins = moins.emission;
            em_Moins.rateOverTime = 0;


            timerAspiring += Time.deltaTime;//incrementation du temps d'aspiration

            if (scoreSpeed <= 0)
            {
                //emet les particules moins
                em_Moins.rateOverTime = -scoreSpeed;
            }
            else
            {
               //emet les particules plus
                em_Plus.rateOverTime = scoreSpeed;
            }
        }
        else
        {
            //reset des emmisions
            var em_Plus = plus.emission;
            em_Plus.rateOverTime = 0;
            var em_Moins = moins.emission;
            em_Moins.rateOverTime = 0;

            timerAspiring -= Time.deltaTime * 5f;//décrémentation du temps d'aspiration
        }
        timerAspiring = Mathf.Clamp(timerAspiring, 0, timerMax + 1);//clamp du timer en 0 surtout



        if (!isFleeing) {//si pas en fuite
            if (timerAspiring >= timerMax)
            {
                //action de fuir si au dela de timermaw
                ChangeAtionTo("isFleeing");
                speed = fleepSpeed;
                FleeAction();

            }
            else if (timerAspiring > timerMax - timerWarning)
            {
                //warning si compris dans les timerMaw et timerWarning
                ChangeAtionTo("isWarning");
                if (warningCoroutine == null)
                {
                    warningCoroutine = StartCoroutine(DoWarning());//coroutine du warning pour le son
                }
            } else if(timerAspiring > 0.1f)
            {
                //anim de l'aspiration en aspirant
                ChangeAtionTo("isAspiring");
            }
            else
            {
                //reset des anims
                ChangeAtionTo();
            }
        }

    }

    /// <summary>
    /// afficher le warning sur le son
    /// </summary>
    /// <returns></returns>
    IEnumerator DoWarning()
    {
        warningSource.Play();
        yield return new WaitForSeconds(timerWarning);
        warningCoroutine = null;
    }

    /// <summary>
    /// Raccourcis pour changer d'action et d'animation
    /// </summary>
    /// <param name="nameBool"></param>
    public void ChangeAtionTo(string nameBool = "Walk")
    {
        int CaseInt = -1;
        if(nameBool == "isAspiring")
        {
            CaseInt = 0;
        }else if (nameBool == "isWarning")
        {
            CaseInt = 1;
        }
        else if (nameBool == "isFleeing")
        {
            CaseInt = 2;
        }

        isFleeing = CaseInt == 2 ? true : false;
        isWarning = CaseInt == 1 ? true : false;
        //isAspiring = CaseInt == 0 ? true : false;
        animator.SetBool("isFleeing", CaseInt == 2 ? true : false);
        animator.SetBool("isWarning", CaseInt == 1 ? true : false);
        animator.SetBool("isAspiring", CaseInt == 0 ? true : false);

    }

    /// <summary>
    /// Fonction appellée à la fuite
    /// </summary>
    public void FleeAction()
    {
        GameManager.Instance.AddScore(-MalusScore);//malus de score

        if (shakeCoroutine == null) {
            shakeCoroutine = StartCoroutine(DoShake());//shake de camera
        }
        if (isKilt)
        {
            GameManager.Instance.maxTimer += 30;//Si kilt alors gain de 30 secondes
        }

        Onomatop.Instance.DoOnomatopDown();//afficher l'onomatopé


        StartCoroutine(DoScream());//cri
    }

    IEnumerator DoScream()
    {
        screamSource.Play();
        yield return new WaitForSeconds(screamSource.clip.length);
    }

    /// <summary>
    /// Shake la caméra et attend
    /// </summary>
    /// <returns></returns>
    public IEnumerator DoShake()
    {
       yield return Camera.main.DOShakePosition(0.5f).WaitForCompletion();
        shakeCoroutine = null;

    }

    private void OnTriggerStay2D(Collider2D other)
    {
        
        if (other.CompareTag("Player"))
        {
            if (PlayerController.Instance.force > 0.1f)
            {
                isAspiring = true;//Si on souffle, on aspire
                GameManager.Instance.AddScore(Time.deltaTime * timerAspiring * scoreSpeed);//incrementation du score
                
            }
            else
            {
                isAspiring = false;//si on souffle pas
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            isAspiring = false;//si on sort
        }
    }

    /// <summary>
    /// Fonction de customization de toutes les parties de la fille en y assignant chaque parties
    /// </summary>
    /// <param name="spr_body"></param>
    /// <param name="spr_leg"></param>
    /// <param name="spr_calf"></param>
    /// <param name="spr_shoes"></param>
    /// <param name="spr_skirt"></param>
    /// <param name="spr_arm"></param>
    public void CustomizeWalker(Sprite spr_body, Sprite spr_leg, Sprite spr_calf, Sprite spr_shoes, Sprite spr_skirt, Sprite spr_arm) {

        body.sprite = spr_body;
        leg1.sprite = spr_leg;
        calf1.sprite = spr_calf;
        shoes1.sprite = spr_shoes;
        leg2.sprite = spr_leg;
        calf2.sprite = spr_calf;
        shoes2.sprite = spr_shoes;
        skirt.sprite = spr_skirt;
        arm1.sprite = spr_arm;
        arm2.sprite = spr_arm;
    }

    
}
