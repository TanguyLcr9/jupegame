﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreDisplay : MonoBehaviour
{
    public TextMeshProUGUI Score;
     public TextMeshProUGUI Number;


    private void Start()
    {

        Score.text = "";
        Number.text = "";


        for (int i = 0; i < ScoreSaver.Instance.scoresData.Length; i++)
        {



            if (ScoreSaver.Instance.scoresData[i] == null)
            {// si il n'y en a pas
                Number.text += " ";//rang du score
                Score.text += " ";//0 par défault
                if(i == 0)
                {
                    Number.text += "No Highscores ";
                }

            }
            else
            {
                Number.text += "N " + (i + 1) + "\n";//rang du score
                Score.text += ScoreSaver.Instance.scoresData[i].score + "\n";//du meilleur score au 20e score
            }
        }
    }
}
