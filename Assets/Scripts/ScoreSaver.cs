﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using NaughtyAttributes;


public class ScoreSaver : MonoBehaviour
{
    public static ScoreSaver Instance;
    public int nombreDeScore = 20;
    public ScoresData[] scoresData;
    int[] scoreSaved;
    int newRange = -1;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);//si instance existe, on détruit pour eviter problèmes
        }
        else
        {
            Instance = this;//Si aucune instance, celle ci est l'instance
        }

        scoresData = new ScoresData[nombreDeScore];//initialisation du tableau des scores

        try
        {
            if (File.Exists(Application.persistentDataPath + "/scoring.dat"))//si la sauvegarde existe
            {
                BinaryFormatter decoder = new BinaryFormatter();
                FileStream stream = File.OpenRead(Application.persistentDataPath + "/scoring.dat");
                ScoresData[] data = (ScoresData[])decoder.Deserialize(stream);

                //on charge le tableau des scores
                scoresData = data;

                stream.Close();
                Debug.Log("Sauvegarde chargée");
            }
            else
            {
                Debug.Log("'scoring.dat' don't exist");
            }

        }
        catch (System.Exception)
        {
            Debug.Log("Probleme occured when opening saves");
        }

    }

    [Button]
    public void ResetDatas()
    {
        ScoresData[] newSave = new ScoresData[nombreDeScore];

        try
        {
            //sauvegarde
            ScoresData[] data;
            data = newSave;


            BinaryFormatter encoder = new BinaryFormatter();
            FileStream stream = File.OpenWrite(Application.persistentDataPath + "/scoring.dat");
            encoder.Serialize(stream, data);
            stream.Close();
            Debug.Log("Sauvegarde Réussie");
        }
        catch (System.Exception)
        {
            Debug.LogWarning("la sauvegarde à eu un problème");
        }
    }

    [Button]
    void SaveDebug()
    {
        SaveScore(100);
    }
    /// <summary>
    /// Sauvegarde des scores avec arrangement des scores les plus hauts
    /// </summary>
    public void SaveScore(int newScore = 100)
    {

        //Arrangement des scores
        DuplicateScores();

        for (int i = 0; i < scoresData.Length; i++)
        {
            int range = i;
            for (int j = i; j < scoresData.Length; j++)
            {
                if (scoresData[range].score < scoresData[j].score)
                {
                    range = j;
                }
            }
            if (range != i)
            {
                scoresData[i].score = scoreSaved[range];
                scoresData[range].score = scoreSaved[i];
            }

            DuplicateScores();
        }

        //Assignation du nouveau score
        DuplicateScores();
        for (int i = 0; i < scoresData.Length; i++)
        {
            if (newScore > scoresData[i].score)
            {
//                Debug.Log(scoreSaved[i]);
                scoresData[i].score = newScore;
                for (int j = i; j < scoresData.Length - 1; j++)
                {
                    scoresData[j + 1].score = scoreSaved[j];
                }
                newRange = i;//rang dans les 20 derniers
                break;
            }
            newRange = -2;//pas dans la grille des 20 derniers scores
        }

        try
        {
            //sauvegarde
            ScoresData[] data;
            data = scoresData;


            BinaryFormatter encoder = new BinaryFormatter();
            FileStream stream = File.OpenWrite(Application.persistentDataPath + "/scoring.dat");
            encoder.Serialize(stream, data);
            stream.Close();
            Debug.Log("Sauvegarde Réussie");
        }
        catch (System.Exception){
            Debug.LogWarning("la sauvegarde à eu un problème");
        }


    }

    /// <summary>
    /// sauvegarde dans l'autre tableau les scores
    /// </summary>
    void DuplicateScores()
    {
        scoreSaved = new int[scoresData.Length];

        for (int i = 0; i < scoresData.Length; i++)
        {
            scoreSaved[i] = scoresData[i].score;
        }
    }
}

[System.Serializable]
public class ScoresData //class du score à sauvegarder
{
    public int score;
    public int name;
}