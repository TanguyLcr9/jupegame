﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Onomatop : MonoBehaviour
{
    public static Onomatop Instance;
    public Coroutine coroutine;
    // Start is called before the first frame update
    void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
        GetComponent<SpriteRenderer>().DOFade(0, 0);
    }

    public void DoOnomatopDown()
    {
        if(coroutine == null)
        {
            coroutine = StartCoroutine(DoOnomatope());
        }
    }

    public IEnumerator DoOnomatope()//joue l'animation d'onomatopé à l'écran avec le son
    {
        GetComponent<SpriteRenderer>().DOFade(1, 0.2f);

        GetComponent<AudioSource>().Play();

        yield return transform.DOShakeRotation(0.5f).WaitForCompletion();

        GetComponent<SpriteRenderer>().DOFade(0, 0.2f);
        coroutine = null;

    }
}
