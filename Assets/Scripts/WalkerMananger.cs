﻿using System.Collections;
using UnityEngine;
using UnityEngine.Rendering;
using System;
using NaughtyAttributes;

[Serializable]
public class LevelData
{
    public float girlReactivity;
    public float timeSpawn;
    public float[] frequenceType;

}

public class WalkerMananger : MonoBehaviour
{
    public static WalkerMananger Instance;//Instance staique pour récuperer les fonctions du script dans les autres

    [Tooltip("Temps entre chaque spawn, initalement la valeur indiqué dans l'inspecteur, ne doit pas être inférieur à 1")]
    [Range(1f,20f)]
    public float TimeBetweenSpawn = 1;
    public GameObject[] Walkers;
    public Transform spawnerRight, spawnerLeft;
    public Sprite[] body, leg, calf, shoes, skirt, arm;
    //Variables pour randomiser la vitesse, et l'écart de position des marcheurs
    public Vector2Int speedRange_v2;
    public Vector2 profondRange_v2;
    public LevelData[] LevelParam;
    public int currentLevel;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);//si instance existe, on détruit pour eviter problèmes
        }
        else
        {
            Instance = this;//Si aucune instance, celle ci est l'instance
        }
    }

    // Start is called before the first frame update
    void OnEnable()
    {
        StartCoroutine(Spawning());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // instantier une fille selon un temps
    IEnumerator Spawning() {

        while (true)
        {
            InstantiateGirl();//instanciation
            yield return new WaitForSeconds(TimeBetweenSpawn + UnityEngine.Random.Range(-1f,1f));//attente
        }

    }

    void InstantiateGirl(int type =0)
    {
        TimeBetweenSpawn = LevelParam[currentLevel].timeSpawn;//temps entre chaque spawn en fonction du niveau

        float randomiseWalker = UnityEngine.Random.Range(0, 103f);//pourcentage random pour déterminer le walker
        float lastFrequence = 0;//frequence du type de walker avant, en partant de 0%

        for (int i = 0; i < LevelParam[currentLevel].frequenceType.Length; i++)
        {
            
            if (lastFrequence < randomiseWalker && randomiseWalker < LevelParam[currentLevel].frequenceType[i])
            {
                type = i;//si lé pourcentage est entre les deux fréquences alors c'est tel type
                break;
            }
            if(randomiseWalker> 100f)
            {
                type = Walkers.Length - 1;//si au dela de 100 alors c'est un porteur de kilt, très rare et tout le temps possible
            }
            lastFrequence = LevelParam[currentLevel].frequenceType[i];//cette frequence actuelle devient la précédente pour l'itération suivante
        }

        int ran = UnityEngine.Random.Range(0, 2);//randomise le spawn position entre la droite et la gauche

        GameObject newGirl = Instantiate(
            Walkers[type],
            (ran == 0 ? spawnerLeft.position : spawnerRight.position) + new Vector3(0,  0, UnityEngine.Random.Range(profondRange_v2.x, profondRange_v2.y)),
            ran == 0 ? Quaternion.identity : Quaternion.Euler(new Vector3(0, 180, 0)),
            transform);//instanciation en fontion de tout ce qui précede, le type est random et la position aussi, ainsi que la profondeur

        WalkerBehaviour behaviour = newGirl.GetComponent<WalkerBehaviour>();
        behaviour.InverseWalk = ran == 0 ? true : false;//walk inverse en fonction du spawn;
        behaviour.timerMax = LevelParam[currentLevel].girlReactivity;//réactivité en fonction du niveau
        behaviour.timerWarning = 1.5f;// temps de warning de la fille en fonction de la réactivité, le warning est actif quand timer < timermax - timerwarning


        newGirl.GetComponent<SortingGroup>().sortingOrder = (int)(profondRange_v2.y*100f - newGirl.transform.position.z*100f);//question d'affichage, il faut que les sprites soient au dessus en fonction de la profondeur

        

        behaviour.speed = UnityEngine.Random.Range(speedRange_v2.x, speedRange_v2.y);
        try
        {
            // fonction non utlisée, mais à la base pour faire en sorte qu'on puisse customiser les filles
            // si on rajoute des sprites au script ça passe
            if (type == 0)
            behaviour.CustomizeWalker(
                body[UnityEngine.Random.Range(0, body.Length)],
                leg[UnityEngine.Random.Range(0, leg.Length)],
                calf[UnityEngine.Random.Range(0, calf.Length)],
                shoes[UnityEngine.Random.Range(0, shoes.Length)],
                skirt[UnityEngine.Random.Range(0, skirt.Length)],
                arm[UnityEngine.Random.Range(0, arm.Length)]
                );
            Debug.Log("Yey");
        }
        catch (System.Exception)
        {

        }
    }
}
