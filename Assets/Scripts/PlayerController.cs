﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;//Instance staique pour récuperer les fonctions du script dans les autres
    public float Range;
    public float move;
    public float force;
    public float speedMove, speedForce;
    bool souffle;
    bool isConnected;
    public ParticleSystem wind;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);//si instance existe, on détruit pour eviter problèmes
        }
        else
        {
            Instance = this;//Si aucune instance, celle ci est l'instance
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (!isConnected)
        {
            //Façon clavier si arduino non connecté
            move = Input.GetAxis("Horizontal");
            transform.position += new Vector3(move * Time.deltaTime * speedMove, 0, 0);
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, -Range, Range), 0, 0);
            if (Input.GetKey(KeyCode.Space))
            {
                force += Time.deltaTime * speedForce;
            }
            else
            {
                force -= Time.deltaTime * speedForce;
            }
        }
        else
        {
            //Façon Arduino
            if (souffle)//Si on clique sur le boutton
            {
                force += Time.deltaTime * speedForce;
            }
            else
            {
                force -= Time.deltaTime * speedForce;
                
            }
            
        }


        var em = wind.emission;
        //em.enabled = force > 0.1F ? true : false;
        em.rateOverTime = force * 2.5f;
        force = Mathf.Clamp(force, 0, 4);

    }

    /// <summary>
    /// Fonction prévue par Ardity pour lire les messages si il reçoit
    /// </summary>
    /// <param name="msg"></param>
    void OnMessageArrived(string msg)
    {
        ArduinoData json = JsonUtility.FromJson<ArduinoData>(msg);

        transform.position = new Vector3(ExtensionsMethods.Remap(json.slider,0,40,Range,-Range), 0, 0);// déplacement du curseur en fonction du déplacement du pistolet
        souffle = json.button;
        Debug.Log(souffle);

    }

    /// <summary>
    /// Fonction prévue par Ardity pour valider la connexion
    /// </summary>
    /// <param name="success"></param>
    void OnConnectionEvent(bool success)
    {
        isConnected = success;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(new Vector3(-Range,0,0), new Vector3(Range, 0, 0));//Gizmo pour voir depuis l'editor les déplacements max du pointeur
    }
    
}

public static class ExtensionsMethods
{

    //Method de remap de valeur car unity n'en as pas de base
    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

}

//class pour recupérer les variables du json;
public class ArduinoData
{
    public int slider;
    public bool button;
}
